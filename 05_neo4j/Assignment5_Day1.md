# Assignment 5 - Day1

See DAY1.md for instructions on how to run Neo4J on your local system
rather than in GitPod.io.

You may also modify this file locally. However, to submit this file,
you'll need to get this into your personal course repository on GitLab.
There are many ways to do this. The way that is closest to our normal
workflow is as follows: open your repository in GitPod then drag this file
from your local machine into 05_neo4j/ in GitPod (replacing the one there),
and then synch your GitPod workspace with your repository on GitLab as normal.

## Find

1. Browse through the Neo4j docs at https://neo4j.com/docs and read more about
    Cypher syntax. Find some Cypher features mentioned in those docs that
    we didn’t have a chance to use here and pick your favorite. Paste link here.

    ```

    ```

2. Experiment with an example graph consisting of movie-related data by
    going back to the browser console at http://localhost:7474/browser,
    typing :play movie-graph into the web console, and follow the instructions.
    (Nothing to submit here.)

    ```

    ```

## Do

1. Create a simple graph describing some of your closest friends, your
    relationships with them, and even some relationships between your friends.
    Start with three nodes, including one for yourself, and create five
    relationships. Paste the commands you used to do this.

    ```

    ```
