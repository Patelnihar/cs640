# Day 1 - Neo4J

Neo4J comes with a web application for manipulating a database. But it
requires custom network schemes that GitPod.io does not support. So, we'll
need to run Neo4J locally to try out its web-interface.

Use these instructions to work through Neo4J day 1 in your text.

## Docker Desktop

1. Install Docker Desktop or use a machine that has docker installed.
2. Start Docker Desktop, if it's not already running (Look for Docker icon in
    your system tray).

    ![docker icon](images/docker.png)

## Download a zip of your repository

![download](images/download.png)

Download it somewhere you know how to find.

## Extract the zip

![extract](images/extract.png)

## Position a terminal in 05_NEO4J

On Windows, use power shell (unless you know better).

![position terminal](images/position-terminal.png)

## Start DB

On Mac/Linux

```bash
./up.bash
```

On Windows, you may not have permissions to run the script below. If that's the case,
edit this file, and then copy-paste each command into powershell to run them.

```powershell
./up.ps1
```

On Windows, you may not have permissions to run scripts. If that's the case,
edit this file, and then copy-paste each command into powershell to run them.

## Clean DB

This step deletes logs/ and data/ directory which are created by the database.
This step may not be necessary when working locally. But you'll need it later
when working in GitPod.io.

I only have instructions for Mac/Linux. Hopefully Windows won't need this.

Before stopping your DB

```bash
./clean.bash
```

## Stop DB

On Mac/Linux

```bash
./stop.bash
```

On Windows, you may not have permissions to run the script below. If that's the case,
edit this file, and then copy-paste each command into powershell to run them.

```powershell
./down.ps1
```
