# Assignment 3 - Mongo Day 2

## Day 2 Homework

Find

1. Find a shortcut for admin commands. Write the shortcut here.

2. Find the online documenation for queries and cursors. Write the URL here.

3. Find the MongoDB documentation for mapreduce. Write the URL here.

4. Through the JavaScript interface, investigate the code for three collections
    functions: help(), findOne(), and stats(). Past the code for each below.
    For each, write a one-sentance insight that you learned by looking at
    the code.

