# Assignment 3 - Mongo Day 3

THIS IS OPTIONAL

Complete this part if you want a challenge. Even if you don't get it working,
you will learn a lot. Feel free to make use of my office hours (or book me)
for help.

Create and interact with a Mongo ReplicaSet. To get started:

1. Create a new directory named replicaset and work in that directory.
2. Copy docker-compose.yml from 03_mongodb into that directory.

Now comes the hard part... Research how to update the docker-compose.yml file
to create a replica set. Below is the docker compose file with comments
explaining what the different parts are for.


```docker-compose.yml

version: '3.1'

# This section lists each node.
services:

  # Here we define a service named "mongo".
  mongo:

    # Service name and the container_name must be unique.
    # They may be used to identify this machine on the network (i.e., in a URL).
    container_name: 7dbs_mongo_db

    # The version of mongo that this machine will run.
    image: mongo:4.0

    # If the machine craches, "always" will start a new one.
    restart: always

    # Variables to pass to the machine. Use to configure it.
    environment:
      MONGO_INITDB_ROOT_USERNAME: mongo
      MONGO_INITDB_ROOT_PASSWORD: gonmo
```

You'll need to define two more services, and you may need to configure
them different. Search the Web for documentation and tutorials on how to
configure a mongo replicaset using docker and/or docker-compose. This
is the challenge.

AGAIN, THIS IS OPTIONAL