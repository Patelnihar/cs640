#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "${SCRIPT_DIR}"

set -euo pipefail

if [ $# -lt 1 ] || [ -z "$1" ] ; then
    >&2 echo "usage: $0 DIRECTORY_NAME"
    exit 1
fi

if [ ! -d "$1" ] ; then
    >&2 echo "Directory does not exist: $1"
    exit 1
fi

if docker ps -f name=7dbs_mongo_db | grep 7dbs_mongo_db > /dev/null ; then
    >&2 echo "Please stop the DB first."
    exit 1
fi


sudo rm -rf ./.data/
sudo cp -R "$1" ./.data/
