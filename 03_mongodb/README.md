
## Start the database


```
./up.bash
```

## Stop the database

It's data will be lost. See run-script.bash for a way to "save" your database.

```
./down.bash
```

## Connect to the database

```
./shell.bash
```

## Run a script in the database

```
./run.bash < script-file.js
```

## Saving your database

Mongo is cabable of live backups and restores. We won't be using these.
Instead, write a set of scripts that insert data into collections, and
query those collections. These scripts are files that you can save in
your repository.

I suggest creating a `load.js` script that inserts starter data into
the running database. To load it run

```
./run.bash < load.js
```

Then you can either connect to the database with `shell.bash` or write
and run other scripts to test out various commands.
