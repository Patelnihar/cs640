# CouchDB

## Start the database

```bash
./up.bash
```

The above will show instructions for accessing Fauxton and using `curl`
to interact with the database.

## Stop and remove the database

```bash
./down.bash
```

## Bash Scripts

The text demonstrates how to use `curl` to make requests to a CouchDB.
Because `curl` is a command-line tool, we can create bash scripts to populate
our database with test data. See `example-load-data.bash` for an example.
