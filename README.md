# 7 Databases in 7 Weeks

This repository contains Docker environments to help you work through "Seven Databases in Seven Weeks".
Details on the book are available on [The Pragmatic Programmers](https://pragprog.com/book/rwdata/seven-databases-in-seven-weeks) website.

The database scripts are based on those in https://github.com/usmanatron/docker-7Databases ,
which are distributed under the MIT License.

## Set up to work

1. Navigate to your personal repository on GitLab (given to you by your instructor).
2. In front of the URL, type `https://gitpod.io/#` and press enter.
3. Start a new workspace, accepting the default configuration.

## Work

See the README.md for specific instructions for that database.

## Synchronize your work with your GitLab repository

```bash
$GITPOD_REPO_ROOT/bin/synch.bash "a short message"
```
